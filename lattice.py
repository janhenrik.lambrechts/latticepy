import sys
import operator as op
from functools import reduce
import math

################################################################
# References                                                   #
# [1] Lattice Path and Counting, Gopal Mohanty                 #
# [2] The Enumeration of Lattice Paths and Walks, Shanzhen Gao #
# [3] Lattice Paths Combinatorics, Michael Wallner             #
# [4] Lattice Path Enumeration, Christian Krattenhaler         #
################################################################


def comb(n, r):
    """
    General nCr Function that runs efficiently on both Python2 & Python3
    """
    r = min(r,n-r)
    numer = reduce(op.mul, range(n, n-r, -1), 1)
    denom = reduce(op.mul, range(1, r+1), 1)
    return numer / denom


def comb_(n, k):
    """
    Helper function that extends comb to return 0 for impossible combinations
    """
    if k < 0:
        return 0
    elif k > n:
        return 0
    else:
        return comb(n,k)


def get_amount_simple_paths_without_restrictions(x1, y1, x2, y2):
    """
    Returns the amount of simple lattice paths from (x1, y1) -> (x2, y2) without
    restrictions based on [4]
    """
    return comb(x2+y2-x1-y1, x2-x1)


def get_amount_simple_paths_fixed_steps(x1, y1, x2, y2, n):
    """
    Returns the amount of lattice paths from (x1, y1) -> (x2, y2) with
    n horizontal and vertical unit steps (in positive or negative direction), see [4]
    """
    s1 = comb(n, (n+x2+y2-x1-y1)/2)
    s2 = comb(n, (m+x2-y2-x1+y1)/2)
    return int(s1*s2)
    

def get_amount_simple_paths_weakly_x_y(x1, y1, x2, y2):
    """
    Based on [4], returns the amount of simple lattice paths starting from the origin (x1,y1) 
    to (x2,y2) that stay weakly below the y = x line.
 
    """
    s1 = comb(x2+y2-x1-y1, x2-x1) 
    s2 = comb(x2+y2-x1-y1, x2-y1+1)
    return int(s1-s2)




def get_amount_simple_paths_from_origin_no_x_y(m,n,t):
    """
    Based on [1,2], returns the amount of simple lattice paths starting from the origin (0,0) 
    to (m,n) that do not touch the x = y+t line if m > n+t. 
    If m < n+t then we necessisarily have to cross x=y+t and as such the amount of paths is 
    equal to 0.

    Note: This is for strongly below the line, x=y+t. For the weakly version, please use:
    def get_amount_simple_paths_weakly_x_y().
    """
    if m < 0 or n < 0: # only up or right movements cannot get you to lower than (0,0)
        return 0

    if m > n+t: 
       return comb((m+n),n) - comb_((m+n), (m-t))
    else: 
        return 0



def get_amount_paths_between_two_slopes(x1, y1, x2, y2, s, t):
    """
    Number of all paths from (x1,y1) -> (x2, y2) staying weakly below the line y = x+t and
    above the line y = x+s, as by [4]
    """
    assert x1 + t >= y1
    assert y1 >= x1+s
    assert x2 + t >= y2
    assert y2 >= x2 + s
    sum_ = 0
    k_max = int((t-s+1)/2)
    for k in range(1, k_max+1):
        sum_+= (4*(2*math.cos(math.pi*k / (t-s+2)))**(x2+y2-x1-y1)/(t-s+2)) * math.sin(math.pi*k*(x1-y1+t+1)/(t-s+2)) * math.sin(math.pi*k*(x2-y2+t+1)/(t-s+2))
    return int(sum_)

def absolute_to_relative_coordinates(x1, y1, x2, y2):
    """Helper function that transforms a problem 
    from absolute coordinates:  (x1, y1) -> (x2, y2)
    to relative coordinates: (0, 0) -> (m, n).

    This is often a necessary conversion before using certain formulas and conditions"""

    x_delta = 0-x1
    y_delta = 0-y1
    m = x2 + x_delta
    n = y2 + y_delta
    
    return (m, n)



def get_amount_of_paths_avoiding_three_consecutive_moves(m, n):
    """
    Returns the number of all simple lattice paths (0,0) -> (m,n)
    that avoids taking three consecutive moves in either the up or right direction.
    - Based on [2]
    """
    s1,s2,s3=0,0,0
    k_max = int(m/2)
    for k in range(m-n-1, k_max):
        s3+=comb(m-k,k)*comb(m-k+1, n-m+k-1)
        if k >= m-n:
            s2+=2*comb(m-k,k)*comb(m-k,n-m+k)
        if k>= m-n+1:
            s3+=comb(m-k,k)*comb(m-k+1, n-m+k-1)
    return int(s1+s2+s3)
    


