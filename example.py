from lattice import *

if __name__ == "__main__":
    # Get amount of steps from (-2, 1) to (5,3) avoiding three consecutive moves
    m, n = absolute_to_relative_coordinates(-2, 1, 5, 5)
    result = get_amount_of_paths_avoiding_three_consecutive_moves(m,n)
    print(result)
