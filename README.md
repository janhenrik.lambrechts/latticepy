# LatticePy: Solving Lattice Paths Problems with Python

This repo brings together a lot of results in lattice paths combinatorics for you to use in your personal projects, we added some examples in example.py. A few of the things that this repocan do:

- Amount of simple lattice paths from ```(x1, y1)``` to ```(x2, y2)``` without restrictions 
- Amount of lattice paths from ```(x1, y1)``` to ```(x_2, y_2)``` with ```n``` horizontal and vertical steps
- Amount of simple lattice paths from ```(x1, y1)``` to ```(x2, y2)``` that stay weakly below the line ```y=x+t``` and weakly above ```y=x+s```.
- Amount of simple lattice paths from ```(0, 0)``` to ```(m, n)``` that avoid taking three consecutive "up" or "right" moves.
- and more...

## References
- Lattice Path Counting and Applications, Gopal Mohanty - First Edition
- The Enumeration of Lattice Paths and Walks, Shanzhen Gao - PhD Thesis 
- Lattice Path Combinatorics, Michael Wallner - Masters Thesis 
- Lattice Path Enumeration, Christian Krattenthaler - Masters Thesis 
